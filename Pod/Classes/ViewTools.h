/**
* ViewTools.h
* 
* \brief     Short description.
* \details   Detailed description.
* \author Kirill (c)
* \date 10.05.14
*/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ViewTools : NSObject
+ (void)embedChildView:(UIView *)child intoParent:(UIView *)parent;
@end
