//
//  TQJSONURLModel.m
//  ThreeQSDN
//
//  Created by Malte Fentroß on 02.07.15.
//  Copyright (c) 2015 Malte Fentroß. All rights reserved.
//

#import "TQJSONURLModel.h"

@implementation TQJSONURLModel

+(BOOL)propertyIsIgnored:(NSString *)propertyName {
    if([propertyName isEqualToString:@"Smooth-Streaming"])
        return YES;
    else
        return NO;
}

@end
