//
//  TQJSONResponse.h
//  ThreeQSDN
//
//  Created by Malte Fentroß on 02.07.15.
//  Copyright © 2015 Malte Fentroß. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "TQJSONURLModel.h"

@interface TQJSONResponse : JSONModel

// the status code (like http status code)
@property (assign, nonatomic) NSInteger Code;

// error messages
@property (strong, nonatomic) NSString<Optional> *Message;

// the internal stream id (optional)
@property (assign, nonatomic) NSInteger StreamId;

// the url to the publishing point of the stream
@property (strong, nonatomic) NSString<Optional> *PublishingPoint;

// the name of the stream / label
@property (strong, nonatomic) NSString<Optional> *StreamName;

// is stream online or nay?
@property (assign, nonatomic) BOOL IsOnline;

// number of active streaming sessions on given stream
@property (assign, nonatomic) NSInteger Listeners;

// the playout url
@property (strong, nonatomic) NSString<Optional> *URI;

// host url for video player
@property (strong, nonatomic) NSString<Optional> *Host;

// original output urls
// of type QJSONURLModel
@property (strong, nonatomic) TQJSONURLModel<Optional> *SourceOutput;

// transcoder output urls
// of type QJSONURLModel
@property (strong, nonatomic) TQJSONURLModel<Optional> *TranscoderOutput;


@end