//
//  TQStreamManager.h
//  ThreeQSDN
//
//  Created by Malte Fentroß on 02.07.15.
//  Copyright © 2015 Malte Fentroß. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TQJSONURLModel.h"

@interface TQStreamManager : NSObject

// init the settings
+(void) initializeWithKey:(NSString *)key;

// Create stream and return the stream id
+(void) createStreamWithLabel:(NSString *)label
                 onCompletion:(void(^)(NSInteger streamID, NSError *error)) block;

// Create stream and return the stream id
// with additional values
+(void) createStreamWithLabel:(NSString *)label
                 withMetaData:(NSDictionary *)metaData
                   autorecord:(BOOL)autorecord
                 onCompletion:(void(^)(NSInteger streamID, NSError *error)) block;

// delete stream that has given stream id
+(void) deleteStreamWithID:(NSInteger)streamID
              onCompletion:(void(^)(NSError *error)) block;

// return data that is required to connect with the rtmp server
+(void) getPublishingPointForStream:(NSInteger)streamID
                       onCompletion:(void(^)(NSString *publishingPoint,
                                             NSString *streamName,
                                             NSError *error))block;

// check if stream is online and return number of viewers
+(void) getStatusOfStream:(NSInteger)streamID
             onCompletion:(void(^)(BOOL isOnline,
                                   NSInteger listeners,
                                   NSError *error))block;

// get all available streaming urls for given stream
+(void) getStreamingURLs:(NSInteger)streamID
            onCompletion:(void(^)(TQJSONURLModel *originalURLs,
                                  NSArray *transcodedURLs,
                                  NSError *error))block;


// set meta data for given stream
// tags is nsarray of nsstring s
+(void) setMetaDataForStream:(NSInteger)streamID
                  parameters:(NSDictionary *)parameters
                        tags:(NSArray *)tags;

@end
