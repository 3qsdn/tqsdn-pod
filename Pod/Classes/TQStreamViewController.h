//
//  TQStreamViewController.h
//  ThreeQSDN
//
//  This VC can be used to connect to a stream and push rtmp data to it.
//  There is a preview view of the stream that can be put somewhere to fit your needs.
//  The VC has multiple controlling functions to start / stop a stream ...
//
//  Created by Malte Fentroß on 02.07.15.
//  Copyright (c) 2015 Malte Fentroß. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCSimpleSession.h"

/**
 * current connection status as enum
 */
typedef NS_ENUM(NSUInteger, TQStreamConnectionStatusType) {
    kTQConnecting,
    kTQDisconnected,
    kTQConnected
};

@protocol TQStreamDelegate <NSObject>

@optional
// tell client that connection status changed
- (void) updatedStreamStatus:(TQStreamConnectionStatusType)status forStream:(NSInteger)streamID;

@end

@interface TQStreamViewController : UIViewController <VCSessionDelegate>

// the current connection status
@property (assign, nonatomic, readonly) TQStreamConnectionStatusType connectionStatus;

// the delegate
@property (strong, nonatomic) id<TQStreamDelegate> delegate;

// initialize with stream id. all necessary data for a connection will be loaded
// on view did load
// TODO: add resolution as well here
- (id) initWithStreamID:(NSInteger)streamID;

// initialize with stream id. all necessary data for a connection will be loaded
// on view did load with given resolution and video and audio quality
- (id) initWithStreamID:(NSInteger)streamID
             withHeight:(CGFloat)height
               andWidth:(CGFloat)width
        andVideoBitrate:(int)bitrate;

// start streaming content of camera
- (void) startStream;

// stop streaming. preview view will stay
- (void) stopStream;

// switch camera
- (void) toggleCamera;

// toggle turn on / off flash light
- (void) toggleFlashlight;

// focus on given point
// (0,0) is top-left, (1,1) is bottom-right
- (void) setFocusOnPoint:(CGPoint)point;

// set zoom factor
- (void) setZoomFactor:(CGFloat)zoom;

@end
