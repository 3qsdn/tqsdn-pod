//
//  TQJSONURLModel.h
//  ThreeQSDN
//
//  Created by Malte Fentroß on 02.07.15.
//  Copyright (c) 2015 Malte Fentroß. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface TQJSONURLModel : JSONModel

@property (strong, nonatomic) NSString *HLS;
@property (strong, nonatomic) NSString *HDS;
@property (strong, nonatomic) NSString *DASH;
@property (strong, nonatomic) NSString<Optional> *RTMP; // is only available in source output
//@property (strong, nonatomic) NSString *Smooth-Streaming;

@end
