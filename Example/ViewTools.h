/**
* ViewTools.h
* 
* \brief     Short description.
* \details   Detailed description.
* \author Kirill (c)
* \date 10.05.14
*/

#import <Foundation/Foundation.h>


@interface ViewTools : NSObject
+ (void)embedChildView:(UIView *)child intoParent:(UIView *)parent;
+ (void)addSideMenuButton:(UIViewController *)viewController;
+ (void) addTopInnerShadowToView:(UIView *)view;
@end
