/**
 * ViewTools.h
 *
 * \brief     Short description.
 * \details   Detailed description.
 * \author Kirill (c)
 * \date 10.05.14
 */

#import "ViewTools.h"
#import <MVYSideMenu/MVYSideMenuController.h>


@implementation ViewTools {
    
}

+ (void)embedChildView:(UIView *)child intoParent:(UIView *)parent
{
    //    [self.view addSubview:parent];
    [parent addSubview:child];
    parent.clipsToBounds = YES;
    
    [child setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLayoutConstraint *top = [NSLayoutConstraint
                               constraintWithItem:child
                               attribute:NSLayoutAttributeTop
                               relatedBy:NSLayoutRelationEqual
                               toItem:parent
                               attribute:NSLayoutAttributeTop
                               multiplier:1.0f
                               constant:0.f];
    NSLayoutConstraint *bottom =[NSLayoutConstraint
                                 constraintWithItem:child
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:0
                                 toItem:parent
                                 attribute:NSLayoutAttributeBottom
                                 multiplier:1.0
                                 constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:child
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:parent
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    NSLayoutConstraint *trailing =[NSLayoutConstraint
                                   constraintWithItem:child
                                   attribute:NSLayoutAttributeTrailing
                                   relatedBy:0
                                   toItem:parent
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0
                                   constant:0];
    
    [parent addConstraint:top];
    [parent addConstraint:bottom];
    [parent addConstraint:leading];
    [parent addConstraint:trailing];
}

+ (void) addTopInnerShadowToView:(UIView *)view {
    UIImageView *innerShadowView = [[UIImageView alloc] initWithFrame:view.bounds];
    
    innerShadowView.contentMode = UIViewContentModeScaleToFill;
    innerShadowView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [view addSubview:innerShadowView];
    
    [innerShadowView.layer setMasksToBounds:YES];
    
    [innerShadowView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [innerShadowView.layer setShadowColor:[UIColor blackColor].CGColor];
    [innerShadowView.layer setBorderWidth:1.0f];
    
    [innerShadowView.layer setShadowOffset:CGSizeMake(0, 0)];
    [innerShadowView.layer setShadowOpacity:1.0];
    
    // this is the inner shadow thickness
    [innerShadowView.layer setShadowRadius:1.5];
}


/**
* we do not need this
*/
+ (void)addSideMenuButton:(UIViewController *)viewController {

    UIButton *button = [[UIButton alloc] init];
    button.titleLabel.text = @"Menu";
    [button addTarget:viewController.sideMenuController action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];

    viewController.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:button];
}

@end
