//
//  main.m
//  ThreeQSDN
//
//  Created by Malte Fentroß on 07/02/2015.
//  Copyright (c) 2014 Malte Fentroß. All rights reserved.
//

@import UIKit;
#import "TQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TQAppDelegate class]));
    }
}
