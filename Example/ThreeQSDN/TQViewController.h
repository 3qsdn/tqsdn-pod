//
//  TQViewController.h
//  ThreeQSDN
//
//  Created by Malte Fentroß on 07/02/2015.
//  Copyright (c) 2014 Malte Fentroß. All rights reserved.
//

@import UIKit;
#import "TQStreamViewController.h"

@interface TQViewController : UIViewController <TQStreamDelegate>

@property (strong, nonatomic) IBOutlet UIButton *startStreamButton;
@property (strong, nonatomic) IBOutlet UIButton *cameraButton;
@property (strong, nonatomic) IBOutlet UIButton *flashButton;

- (IBAction)startStreamButtonTouched:(id)sender;
- (IBAction)toggleFlashlight:(id)sender;
- (IBAction)toggleCamera:(id)sender;

@end
