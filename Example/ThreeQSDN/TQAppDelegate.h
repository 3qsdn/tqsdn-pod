//
//  TQAppDelegate.h
//  ThreeQSDN
//
//  Created by CocoaPods on 07/02/2015.
//  Copyright (c) 2014 Malte Fentroß. All rights reserved.
//

@import UIKit;

@interface TQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
