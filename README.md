# ThreeQSDN



## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ThreeQSDN is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ThreeQSDN"
```

## Author

Malte Fentroß, m.fentross@app-buddy.de

## License

ThreeQSDN is available under the MIT license. See the LICENSE file for more info.